#!/bin/bash

PYTHON_VERSION=${1}

cd /wine/drive_c/Python${PYTHON_VERSION}

for name in python easy_install pip pyinstaller pyupdater
do
  echo "wine 'C:\Python${PYTHON_VERSION}\\${name}.exe'" ' "$@"' > /usr/bin/${name}
done

echo 'assoc .py=PythonScript' | wine cmd
echo "ftype PythonScript=c:\Python${PYTHON_VERSION}\python.exe \"%1\" %*" | wine cmd
while pgrep wineserver >/dev/null; do echo "Waiting for wineserver"; sleep 1; done
chmod +x /usr/bin/python /usr/bin/easy_install /usr/bin/pip /usr/bin/pyinstaller /usr/bin/pyupdater
(pip install -U pip || true)
rm -rf /tmp/.wine-*
