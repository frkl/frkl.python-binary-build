#!/bin/bash

PYTHON_VERSION=${1}

source "${HOME}/.bashrc"

echo "- building Python (version: ${PY_VERSION})"

for msifile in core dev exe lib path pip tcltk tools
do
    wget -nv "https://www.python.org/ftp/python/$PYTHON_VERSION/amd64/${msifile}.msi"
    wine msiexec /i "${msifile}.msi" /qb "TARGETDIR=C:/Python${PYTHON_VERSION}"
    rm ${msifile}.msi;
done

